from typing import BinaryIO, Optional, Type
from types import TracebackType

class PhirosCreator:
    def __init__(self, file: BinaryIO):
        ...
    
    def __enter__(self) -> "PhirosCreator":
        ...

    def __exit__(
        self,
        exc_type: Optional[Type[BaseException]],
        exc_val: Optional[BaseException],
        exc_tb: Optional[TracebackType],
    ) -> None:
        ...

    def prepare(self) -> None:
        ...
    
    def write(self, key: bytes, data: bytes) -> None:
        ...

    def finalize(self) -> None:
        ...
    
class Phiros:
    def __init__(self, file: BinaryIO):
        ...

    def lookup(self, key: bytes) -> bytes:
        ...