swh-phiros: Perfect Hash-Indexed Records for ObjStorage
=======================================================

Building up on swh-perfecthash.

Hacking
-------

Build:

```console
$ maturin develop
```

Test:

```console
$ pytest
```
