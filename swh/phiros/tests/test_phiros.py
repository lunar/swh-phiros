# Copyright (C) 2021-2023 The Software Heritage developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import io
from pathlib import Path

import pytest

from .. import PhirosCreator, Phiros


@pytest.fixture
def phiros_file():
    return open(Path(__file__).parent / "fixtures" / "sample.phiros", "rb")


@pytest.fixture
def tmp_file(tmp_path):
    return tmp_path / "test.phiros"


@pytest.fixture
def creator(tmp_file):
    return PhirosCreator(tmp_file.open("wb"))


def test_creator_all_methods(tmp_file, phiros_file):
    with tmp_file.open("wb") as writable:
        creator = PhirosCreator(writable)
        creator.prepare()
        creator.write(b"one", b"somewhere")
        creator.write(b"two", b"over")
        creator.write(b"three", b"the")
        creator.write(b"four", b"rainbow")
        creator.finalize()
    assert tmp_file.read_bytes() == phiros_file.read()


def test_creator_context_manager(tmp_file, phiros_file):
    with tmp_file.open("wb") as writable:
        with PhirosCreator(writable) as creator:
            creator.write(b"one", b"somewhere")
            creator.write(b"two", b"over")
            creator.write(b"three", b"the")
            creator.write(b"four", b"rainbow")
    assert tmp_file.read_bytes() == phiros_file.read()


def test_creator_calling_prepare_twice_fails(creator):
    creator.prepare()
    with pytest.raises(AssertionError, match="prepare()"):
        creator.prepare()


def test_creator_writing_twice_the_same_key_fails(creator):
    creator.prepare()
    creator.write(b"key", b"data")
    with pytest.raises(ValueError):
        creator.write(b"key", b"something")


def test_lookup(phiros_file):
    phiros = Phiros(phiros_file)
    assert phiros.lookup(b"one") == b"somewhere"
    assert phiros.lookup(b"two") == b"over"
    assert phiros.lookup(b"three") == b"the"
    assert phiros.lookup(b"four") == b"rainbow"
