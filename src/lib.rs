// Copyright (C) 2021-2023 The Software Heritage developers
// See the AUTHORS file at the top-level directory of this distribution
// License: GNU General Public License version 3, or any later version
// See top-level LICENSE file for more information

use binrw::{binrw, binwrite, BinRead, BinWrite};
use ph::fmph;
use pyo3::pycell::PyRefMut;
use pyo3::{
    exceptions::{PyAssertionError, PyRuntimeError, PyValueError},
    prelude::*,
    types::PyBytes,
};
use pyo3_file::PyFileLikeObject;
use std::collections::HashMap;
use std::{
    fmt,
    io::{Seek, SeekFrom},
    vec::Vec,
};

const _PHIROS_HEADER_POSITION: u64 = 32;
const _PHIROS_OBJECT_ENTRIES_POSITION: u64 = 512;

#[binrw]
#[brw(big, magic = b"SWHPhiros1")]
struct PhirosHeader {
    #[brw(seek_before = SeekFrom::Start(32))]
    index_position: u64,
    hash_position: u64,
}

#[derive(Clone)]
#[binrw]
#[brw(big)]
struct Position(u64);

#[binwrite]
#[bw(big)]
struct PositionIndex(Vec<Position>);

#[binrw]
#[brw(big)]
struct ObjectEntry {
    size: u64,

    #[br(count = size)]
    data: Vec<u8>,
}

#[derive(Eq, Hash, PartialEq)]
struct PhirosKey(Vec<u8>);

impl fmt::Display for PhirosKey {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let str = self
            .0
            .iter()
            .map(|b| format!("{:02x}", b))
            .collect::<String>();

        write!(f, "0x{}", str)
    }
}

#[pyclass]
struct PhirosCreator {
    file: PyFileLikeObject,
    /// Mapping from key to positions in the file of the matching object entry
    positions: HashMap<PhirosKey, u64>,
}

#[pymethods]
impl PhirosCreator {
    #[new]
    fn new(obj: PyObject) -> PyResult<Self> {
        let mut file = PyFileLikeObject::with_requirements(
            obj, /* read */ false, /* write */ true, /* seek */ true,
        )?;
        let _ = file.seek(SeekFrom::Start(0))?;
        Ok(PhirosCreator {
            file,
            positions: HashMap::new(),
        })
    }

    fn __enter__<'p>(mut slf: PyRefMut<'p, Self>, _py: Python<'p>) -> PyResult<PyRefMut<'p, Self>> {
        slf.prepare()?;
        Ok(slf)
    }

    fn __exit__(
        &mut self,
        py: Python,
        exc_type: PyObject,
        _exc_value: PyObject,
        _traceback: PyObject,
    ) -> PyResult<()> {
        if !exc_type.is_none(py) {
            return Ok(());
        }
        self.finalize()?;
        Ok(())
    }

    fn prepare(&mut self) -> PyResult<()> {
        if self.file.seek(SeekFrom::Current(0))? != 0 {
            return Err(PyAssertionError::new_err(
                "prepare() has already been called",
            ));
        }
        self.file
            .seek(SeekFrom::Start(_PHIROS_OBJECT_ENTRIES_POSITION))?;
        Ok(())
    }

    fn write(
        &mut self,
        #[pyo3(from_py_with = "key_from_pybytes")] key: PhirosKey,
        bytes: &PyBytes,
    ) -> PyResult<()> {
        let position = self.file.seek(SeekFrom::Current(0))?;
        if position < _PHIROS_OBJECT_ENTRIES_POSITION {
            return Err(PyAssertionError::new_err("prepare() has not been called"));
        }
        if self.positions.contains_key(&key) {
            return Err(PyValueError::new_err(format!(
                "Key {} has already been used",
                key
            )));
        }
        self.positions.insert(key, position);
        let entry = ObjectEntry {
            size: u64::try_from(bytes.len()?)?,
            data: bytes.as_bytes().to_vec(),
        };
        entry
            .write(&mut self.file)
            .map_err(|e| PyRuntimeError::new_err(format!("write error: {}", e)))?;
        Ok(())
    }

    fn finalize(&mut self) -> PyResult<()> {
        if self.positions.len() == 0 {
            return Err(PyAssertionError::new_err("write() has never been called"));
        }

        let keys: Vec<&Vec<u8>> = self.positions.keys().map(|k| &k.0).collect();
        let phf = fmph::Function::from(keys);
        let index_position = self.file.seek(SeekFrom::Current(0))?;
        self.write_index(&phf)?;
        let hash_position: u64 = self.file.seek(SeekFrom::Current(0))?;
        phf.write(&mut self.file)
            .map_err(|e| PyRuntimeError::new_err(format!("write error: {}", e)))?;
        let _ = self.file.seek(SeekFrom::Start(0))?;
        let header = PhirosHeader {
            index_position,
            hash_position,
        };
        header
            .write(&mut self.file)
            .map_err(|e| PyRuntimeError::new_err(format!("write error: {}", e)))?;
        Ok(())
    }
}

// Rust-only methods
impl PhirosCreator {
    fn write_index(&mut self, phf: &fmph::Function) -> PyResult<()> {
        let mut index: Vec<Position> = vec![Position(0); self.positions.len()];
        for (key, entry_position) in self.positions.iter() {
            let offset: u64 = phf
                .get(&key.0)
                .ok_or(PyAssertionError::new_err("key not in phf"))?;
            index[usize::try_from(offset)?] = Position(*entry_position);
        }
        let position_index = PositionIndex(index);
        position_index
            .write(&mut self.file)
            .map_err(|e| PyRuntimeError::new_err(format!("write error: {}", e)))?;
        Ok(())
    }
}

#[pyclass]
struct Phiros {
    file: PyFileLikeObject,
    phf: fmph::Function,
    index_position: u64,
}

#[pymethods]
impl Phiros {
    #[new]
    fn new(obj: PyObject) -> PyResult<Self> {
        let mut file = PyFileLikeObject::with_requirements(
            obj, /* read */ true, /* write */ false, /* seek */ true,
        )?;
        let _ = file.seek(SeekFrom::Start(0))?;
        let header = PhirosHeader::read(&mut file)
            .map_err(|e| PyRuntimeError::new_err(format!("read error: {}", e)))?;
        let _ = file.seek(SeekFrom::Start(header.hash_position))?;
        let phf = fmph::Function::read(&mut file)?;
        Ok(Phiros {
            file,
            phf,
            index_position: header.index_position,
        })
    }

    fn lookup<'p>(
        &mut self,
        py: Python<'p>,
        #[pyo3(from_py_with = "key_from_pybytes")] key: PhirosKey,
    ) -> PyResult<&'p PyBytes> {
        let entry_position = self.lookup_entry_position(key)?;
        println!("entry_position {:x}", entry_position);
        let _ = self.file.seek(SeekFrom::Start(entry_position))?;
        let entry = ObjectEntry::read(&mut self.file)
            .map_err(|e| PyRuntimeError::new_err(format!("read error: {}", e)))?;
        Ok(PyBytes::new(py, &entry.data))
    }
}

// Rust-only methods
impl Phiros {
    fn lookup_entry_position(&mut self, key: PhirosKey) -> PyResult<u64> {
        let offset = self
            .phf
            .get(&key.0)
            .ok_or(PyAssertionError::new_err("key not in phf"))?;
        let _ = self.file.seek(SeekFrom::Start(
            self.index_position + offset * u64::try_from(std::mem::size_of::<Position>())?,
        ))?;
        let position = Position::read(&mut self.file)
            .map_err(|e| PyRuntimeError::new_err(format!("read error: {}", e)))?;
        Ok(position.0)
    }
}

fn key_from_pybytes(obj: &PyAny) -> PyResult<PhirosKey> {
    let bytes: &PyBytes = obj.downcast()?;
    Ok(PhirosKey(bytes.as_bytes().to_vec()))
}

/// A Python module implemented in Rust.
#[pymodule]
#[pyo3(name = "_swh_phiros")]
fn swh_phicos(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<PhirosCreator>()?;
    m.add_class::<Phiros>()?;
    Ok(())
}